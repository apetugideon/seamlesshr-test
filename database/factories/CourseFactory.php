<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Course;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1,50),
        'course_title' => $faker->text(),
        'duration' => $faker->numberBetween(3,12),
        'text' => $faker->text(),
    ];
});
