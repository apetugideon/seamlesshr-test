<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::middleware('jwt.auth')->get('users', function () {
    return auth('api')->user();
});

//Endpoint for User registration
Route::post('register', 'Auth\RegisterController@register');

//Endpoint for User login
Route::post('login', 'Auth\LoginController@login');

/**
*Endpoint to export all courses with the Maatexcel 
*(any version) package in excel or csv format.
*/
Route::get('download/courses', 'CoursesController@export');

/**
 * Create an endpoint to call a course factory to create 50 courses. We want you to queue this operation in a job.
 */
Route::post('courses50', 'CoursesController@populatecourses');

 /** 
 * Create an endpoint for a user to register in one or more courses. 
 */

/** 
 * Create an endpoint to see list of all courses.
 */
Route::get('courses', 'CoursesController@index');

/**
 * If the user is registered in a course, 
 * return the date enrolled in that course.
 */
Route::get('registered_courses', 'CoursesController@registered_courses');

/**
 *  Endpoint for a user to register in one or more courses. 
 */
Route::post('course_registration', 'CoursesController@store');